#!/usr/bin/env bbrun

#%include std/autohelp.sh
#%include std/out.sh

### licensify.sh PATH ... Usage:help
#
# Add a license header to files.
#
# PATH ... is a list of files, or of folders in which to search for source files, to add copyright to.
#
###/doc

extensions_to_cover=(
    py
#    sh
)

license_header_file="$(echo "$(dirname "$0")/license_header.txt")"

$%function lic:rewrite(codefile) {
    if ! grep -Pq -- "\(C\)\s*[0-9]{4}" <(head -n 10 "$codefile"); then
        local tmpfile="$(mktemp)"
        cat "$license_header_file" "$codefile" > "$tmpfile"
        cat "$tmpfile" > "$codefile"
        rm "$tmpfile"
    fi
}

$%function find_appropriate_files(codefile) {
    local extension
    for extension in "$@"; do
        find "$codefile" -type f -name "*.${extension}"
    done
}

function lic:main() {
    local codefile
    local subfile

    for codefile in "$@"; do
        if [[ -f "$codefile" ]]; then
            lic:rewrite "$codefile"

        elif [[ -d "$codefile" ]]; then
            while read subfile; do
                lic:rewrite "$subfile"
            done < <(find_appropriate_files "$codefile" "${extensions_to_cover[@]}")

        elif [[ ! -e "$codefile" ]]; then
            # Non-existent new file
            cat "$license_header_file" > "$codefile"

        else
            out:error "Could not modify/create '$codefile'"
        fi
    done
}

autohelp:check-or-null "$@"
lic:main "$@"
