# Muta Webserver

The Muta web server is an on-demand file-converting server - half way between a static site generator and a full web server. Muta allows you to write your web pages in any markup format you want, whilst not having to perform a site build. The server takes the request, performs the transformation required, and returns a webpage, or other asset, as needed. Server-side scripting is not allowed, so query strings can be leveraged to specify return types and data transformations.

Transformation modules can be added to extend the functionality of Muta, to make it the ideal data server for you.

Muta is best suited for:

* Static informational sites
* Data delivery/display websites
* Artifact repositories

Muta can be configured to enable the Jinja2 templating language to process inclusions and data insertions.

Muta is designed to serve its content as a standalone process. It should not be directly exposed to the internet, but rather should sit behind a robust primary web server like Apache, Nginx or Squid.
